import os

env = Environment()

# Ajusta las flags de compilación
env.Append(CXXFLAGS=['-O3', '-std=c++17', '-Wall', '-Wpedantic'])

# Verifica la variable de entorno SYSTEMROOT para determinar las flags de threading
if os.environ.get('SYSTEMROOT'):
    env.Append(CXXFLAGS=['-lpthread'])
else:
    env.Append(CXXFLAGS=['-pthread'])

# Añade la flag -msse4.2 si SSE4 está habilitado
sse4_enabled = ARGUMENTS.get('SSE4', 0)
if int(sse4_enabled):
    env.Append(CXXFLAGS=['-msse4.2'])

# Define los archivos fuente
sources = Glob('omp/*.cpp')

# Define las reglas de construcción
lib_dir = Dir('lib')
lib_target = lib_dir.File('ompeval.a')

# Asegúrate de que el directorio lib exista antes de construir la biblioteca
env.Command(lib_dir.abspath, '', [Mkdir(lib_dir.abspath)])

lib_ompeval = env.StaticLibrary(target=lib_target, source=sources)  # Modificado aquí

env.Alias(target='all', source=[lib_ompeval, 'test'])

env.Program(target='test', source=['test.cpp', 'benchmark.cpp', lib_ompeval])

# Añade una regla de limpieza
Clean('all', ['test', 'lib/ompeval.a'])
