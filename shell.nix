{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  buildInputs = [
    pkgs.rtags
    pkgs.scons
    pkgs.ccls

    pkgs.cmake
    pkgs.cmake-format
    pkgs.cmake-language-server
  ];
}
